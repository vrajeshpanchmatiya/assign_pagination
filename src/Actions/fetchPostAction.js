import { fetchPostType } from "./Type/fetchPostType";
import { fetchPostService } from "../Services/fetchPostService";
// fetchPostAction for dispatch
export const fetchPostAction = (id) => {
  return async (dispatch) => {
    const details = await fetchPostService(id);
    dispatch({ type: fetchPostType, payload: details.data.hits });
  };
};
