import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchPostAction } from "../Actions/fetchPostAction";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
const FetchPost = () => {
  const dispatch = useDispatch();
  // useSelector for fetching the data
  const detail = useSelector((state) => {
    return state.data;
  });
  //setID with 0
  const [id, setId] = useState(0);
  // useEffect for changing the Id
  useEffect(() => {
    setTimeout(() => {
      dispatch(fetchPostAction(id));
      setId(id + 1);
    }, 10000);
  }, [dispatch, id]);
  // Columns Field for table
  const columns = [
    {
      Header: "Title",
      accessor: "title",
      style: { backgroundColor: "Tomato" },
      sortable: false,
    },
    {
      Header: "URL",
      accessor: "url",
      sortable: false,
      style: { backgroundColor: "salmon" },
    },
    {
      Header: "CREATED_AT",
      accessor: "created_at",
      style: { backgroundColor: "lightgreen" },
      filterable: false,
    },
    {
      Header: "AUTHOR",
      accessor: "author",
      style: { backgroundColor: "lightblue" },
    },
    {
      Header: "ACTION",
      sortable: false,
      filterable: false,
      Cell: (props) => {
        return (
          <Link
            to={{ pathname: "/OriginalData", data: props.original }}
            style={{ textDecoration: "none" }}
          >
            <Button variant="outlined" type="submit">
              ROW JSON
            </Button>
          </Link>
        );
      },
    },
  ];
  //ReactTable component
  return (
    <ReactTable
      columns={columns}
      noDataText="Please Wait"
      showPageSizeOptions={false}
      defaultPageSize={20}
      data={detail}
      filterable
    />
  );
};
export default FetchPost;
