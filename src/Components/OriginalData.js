import React from "react";
const OriginalData = (props) => {
  //Getting data from props
  const info = props.location.data;
  //JSON data
  return <div>{JSON.stringify(info)}</div>;
};
export default OriginalData;
