import { fetchPostType } from "../Actions/Type/fetchPostType";
const initialState = {
  data: [],
};
// Reducer for store
export const fetchPostReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case fetchPostType:
      return {
        ...state,
        //concating new data with old using concat
        data: state.data.concat(payload),
      };
    default:
      return state;
  }
};
