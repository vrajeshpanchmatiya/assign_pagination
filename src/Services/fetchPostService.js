import Axios from "axios";
// Calling of Api
export const fetchPostService = (id) => {
  return Axios.get(`${process.env.REACT_APP_API}${id}`);
};
